<?php 
$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$url = $_SERVER['REQUEST_URI'];
$my_url = explode('wp-content' , $url); 
$path = $_SERVER['DOCUMENT_ROOT']."/".$my_url[0];

include_once $path . '/wp-load.php';

global $wpdb;
	
$coupons_used = $wpdb->get_results('SELECT p.post_date, pm.post_id, pm.meta_value FROM bc_postmeta AS pm LEFT JOIN bc_posts AS p ON p.ID=pm.post_id WHERE pm.meta_key="coupon" AND pm.meta_value<>"" AND pm.post_id IN (SELECT post_id FROM bc_postmeta WHERE meta_key="wpsc_status" AND meta_value="Completed") GROUP BY pm.post_id', ARRAY_A);
	$post_ids = array_column($coupons_used, 'post_id');
	$coupon_names = array_column($coupons_used, 'meta_value');
	
	$coup_count = array();
	
	foreach($coupon_names as $cname)
	{
		if(!isset($coup_count[$cname]))
			$coup_count[$cname] = 1;
		else
			$coup_count[$cname]++;
	}


$goods = $wpdb->get_results('SELECT post_id, meta_value FROM bc_postmeta WHERE meta_key="wpspsc_items_ordered" AND post_id IN ('.implode(',',$post_ids).')', ARRAY_A);
$goods_ids = array_column($goods, 'post_id');
$goods_data = array_column($goods, 'meta_value');
$goods = array_combine($goods_ids, $goods_data);

header("Content-Disposition:attachment;filename=coupons.csv");
header("Content-Type:text/csv;charset=UTF-8");
header("Expires: Mon, 31 Dec 2000 00:00:00 GMT" );
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT" );
header("Cache-Control: post-check=0, pre-check=0", false );

$fp = fopen("php://output", "w");

$headers = array("DATE","COUPON","TIMES USED","PRODUCT ARTICLE(S)");

fputcsv($fp, $headers, ';');



foreach($coupons_used as $coupon)
{
	$good = json_decode($goods[$coupon['post_id']], true);
	$item_numbers = array_column($good, 'item_number');
	$str = array($coupon['post_date'], $coupon['meta_value'], $coup_count[$coupon['meta_value']], implode(', ', $item_numbers));
	fputcsv($fp, $str, ';');
}


		
fclose($fp);


?>