<?php
function wspsc_show_coupon_cstat_settings_page()
{
    if(!current_user_can('manage_options')){
        wp_die('You do not have permission to access this settings page.');
    }
 
    echo '<div class="wrap">';
    echo '<h1>Coupon usage statistics</h1>';
    
    echo '<div id="poststuff"><div id="post-body">';
    
	global $wpdb;
    $coupons_used = $wpdb->get_results('SELECT p.post_date, pm.post_id, pm.meta_value FROM bc_postmeta AS pm LEFT JOIN bc_posts AS p ON p.ID=pm.post_id WHERE pm.meta_key="coupon" AND pm.meta_value<>"" AND pm.post_id IN (SELECT post_id FROM bc_postmeta WHERE meta_key="wpsc_status" AND meta_value="Completed") GROUP BY pm.post_id', ARRAY_A);
	$post_ids = array_column($coupons_used, 'post_id');
	$coupon_names = array_column($coupons_used, 'meta_value');
	
	$coup_count = array();
	
	foreach($coupon_names as $cname)
	{
		if(!isset($coup_count[$cname]))
			$coup_count[$cname] = 1;
		else
			$coup_count[$cname]++;
	}
    
	
	$goods = $wpdb->get_results('SELECT post_id, meta_value FROM bc_postmeta WHERE meta_key="wpspsc_items_ordered" AND post_id IN ('.implode(',',$post_ids).')', ARRAY_A);
	$goods_ids = array_column($goods, 'post_id');
	$goods_data = array_column($goods, 'meta_value');
	$goods = array_combine($goods_ids, $goods_data);
    
    //display table
    $output = "";
    $output .= '
	<form method="POST">
    <table class="widefat" style="max-width:800px;">
    <thead><tr>
    <th scope="col">DATE</th>
    <th scope="col">COUPON</th>
	<th scope="col">TIMES USED</th>
    <th scope="col">PRODUCT ARTICLE(S)</th>  	
    <th scope="col"></th>
    </tr></thead>
    <tbody>';

    
	foreach($coupons_used as $coupon)
	{
		$good = json_decode($goods[$coupon['post_id']], true);
		$item_numbers = array_column($good, 'item_number');
		$output .= '<tr><td>'.$coupon['post_date'].'</td><td>'.$coupon['meta_value'].'</td><td>'.$coup_count[$coupon['meta_value']].'</td><td>'.implode(', ', $item_numbers).'</td></tr>';
	}
	
	
    $output .= '</tbody>
    </table></form>';
	
	$output .= '<div style="margin-top:10px;"><a style="display:block; padding:5px; padding-bottom:15px; text-align:center; width:150px; height:10px; text-decoration:none; border:1px solid #6b7077; background:#fff" href="'.plugins_url().'/wordpress-simple-paypal-shopping-cart/csexport.php">Export CSV</a></div>';

    echo $output;
    wpspsc_settings_menu_footer();
    
    echo '</div></div>';//End of poststuff and post-body
    echo '</div>';//End of wrap
    
}

