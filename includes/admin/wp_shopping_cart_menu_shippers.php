<?php
function wspsc_show_coupon_shippers_settings_page()
{
    if(!current_user_can('manage_options')){
        wp_die('You do not have permission to access this settings page.');
    }
 
    echo '<div class="wrap">';
    echo '<h1>Shippers</h1>';
    
    echo '<div id="poststuff"><div id="post-body">';
    
	global $wpdb;
    $shippers = $wpdb->get_results('SELECT * FROM shippers ORDER BY id ASC', ARRAY_A);
    
    //display table
    $output = "";
    $output .= '
	<form method="POST">
    <table class="widefat" style="max-width:800px;">
    <thead><tr>
    <th scope="col">ID</th>
    <th scope="col">PSEUDO</th>
	<th scope="col">NAME</th>
    <th scope="col">DB TABLE</th>  	
	<th scope="col">FOR COUNTRIES</th>
	<th scope="col">ALTERNATIVE SPEED</th>
	<th scope="col">DEFAULT</th>
    <th scope="col">FREE AFTER</th>
	<th scope="col">FREE FOR ZONE</th>
	<th scope="col">ORDER</th>
	<th scope="col">ACTIVE</th>
    </tr></thead>
    <tbody>';

    
	foreach($shippers as $shipper)
	{
		$defChecked = '';
		$defActive = '';
		
		if($shipper['isdefault'])
			$defChecked = 'checked';
		
		if($shipper['active'])
			$defActive = 'checked';
		
		$output .= '<tr><td>'.$shipper['id'].'</td><td>'.$shipper['pseudo'].'</td><td><input class="tbl" type="text" rel-id="'.$shipper['id'].'" name="name" id="name" value="'.$shipper['name'].'" /></td><td><a href="/wp-admin/admin.php?page=dbte_'.$shipper['prices_table'].'">'.$shipper['prices_table'].'</a></td><td><input type="text" class="tbl" name="restrict_cntrs" value="'.$shipper['restrict_cntrs'].'" rel-id="'.$shipper['id'].'"></td><td><textarea class="tbl" rel-id="'.$shipper['id'].'" name="altershiptime">'.stripslashes($shipper['altershiptime']).'</textarea></td><td><input class="tbl" rel-id="'.$shipper['id'].'" type="radio" name="isdefault" id="isdefault" '.$defChecked.' rel-id="'.$shipper['id'].'" /></td><td><input class="tbl" class="tbl" rel-id="'.$shipper['id'].'" type="text" name="freeonlimit" id="freeonlimit" value="'.$shipper['freeonlimit'].'" /></td><td><input class="tbl" rel-id="'.$shipper['id'].'" type="text" name="freeforzone" id="freeforzone" value="'.$shipper['freeforzone'].'"/></td><td><input class="tbl" rel-id="'.$shipper['id'].'" type="text" name="level" id="level" value="'.$shipper['level'].'"/></td><td><input class="tbl" type="checkbox" rel-id="'.$shipper['id'].'" value="1" name="active" id="active" '.$defActive.' /></td></tr>';
	}
	
	
    $output .= '</tbody>
    </table></form>';
	
	
	$maxId = $wpdb->get_results('SELECT max(id) AS maxid FROM shippers', ARRAY_A);
	$maxId = $maxId[0]['maxid']+1;
	
	$tables = $wpdb->get_results('SELECT pseudo, prices_table, isdefault FROM shippers', ARRAY_A);

	$output .= '<br><br><h3>Add new shipper</h3><br>';
	
	 $output .= '<form method="POST" id="newshipper">
	 <table><tbody>
	 <tr>
	 <td>Pseudo: <br></td>
	 <td><input type="text" name="pseudo" value="shipper'.$maxId.'" /></td>
	  <td>Name:<br> </td>
	 <td><input type="text" name="name" /></td>
	  <td>Db table: <br></td>
	 <td><input type="text" name="prices_table" value="wp_shipping_zones'.$maxId.'" /></td>
	  </tr><tr>
	  <td>Only for c-ries: <br></td>
	 <td><textarea name="restrict_cntrs" cols="20" rows="10"></textarea></td>
	  <td>Alternative ship.time:<br> </td>
	 <td><textarea name="altershiptime" cols="20" rows="10"></textarea></td>
	  <td>Free on limit: <br></td>
	 <td><input type="text" name="freeonlimit" value="0" /></td>
	  </tr><tr>
	  <td>Free for zone:<br> </td>
	 <td><input type="text" name="freeforzone" value="" /></td>
	  <td>Order: <br></td>
	 <td><input type="text" name="level" value="'.$maxId.'"/></td>
	 <td>Copy prices from: <br></td>
	 <td><select name="prices_table_orig">';
	 foreach($tables as $table)
	 {
		 if($table['isdefault'])
			 $selT = 'selected';
		 else
			 $selT = '';
		  $output .= '<option value="'.$table['prices_table'].'" '.$selT.'>'.$table['pseudo'].'</option>';
	 }
	 $output .= '</select></td>
	 </tr><tr>
	 <td><input type="button" name="savesh" id="savesh" value="Save" /></td>
	 </tr>
	 </tbody>
    </table></form>';
	
	$output .= '<br><br><h3>Delete shipper</h3><br>';
	
	$output .= '<form method="POST" id="delshipper">
	 <table><tbody>
	 <tr>
	 <td>Enter ID of shipper you want to delete: <br><td>
	 <td><input type="text" name="idel" /></td>
	 <td><input type="button" name="delsh" id="delsh" value="Delete" /></td>
	 </tr>
	 </tbody>
    </table></form>';
	
    echo $output;
    wpspsc_settings_menu_footer();
    
    echo '</div></div>';//End of poststuff and post-body
    echo '</div>';//End of wrap*/
	
	echo '<script type="text/javascript">
	jQuery(document).ready(function() {
	jQuery("#savesh").on("click", function() {
		var frm = $("#newshipper").serialize();
		jQuery.ajax({
			type: "POST",
			data: "action=newshipper&"+frm,
			url: "https://beavercrafttools.com/wp-admin/admin-ajax.php",
			success: function(data)
			{   
			}
		});
		location.reload();
		});
		})
	</script>';
	
	echo '<script type="text/javascript">
	jQuery(document).ready(function() {
	jQuery("#delsh").on("click", function() {
		var frm = $("#delshipper").serialize();
		jQuery.ajax({
			type: "POST",
			data: "action=delshipper&"+frm,
			url: "https://beavercrafttools.com/wp-admin/admin-ajax.php",
			success: function(data)
			{   
				data = JSON.parse(data);
				if(data.result)
				{
					alert("Shipper deleted");
					location.reload();
				}
				else
				{
					alert("Error occured");
				}
			}
		});
		});
		})
	</script>';
	
	echo '<script>
		$(document).ready(function(){
	  $(".tbl").on("change",function postinput(){ 
		var matchvalue = $(this).val();
		var matchid = $(this).attr("rel-id");
		var relname = $(this).attr("name");
		
		jQuery.ajax({
				type: "POST",
				data: "action=editshipper&val="+matchvalue+"&id="+matchid+"&relname="+relname,
				url: "'.site_url().'/wp-admin/admin-ajax.php",
				success: function(data)
				{   
					data = JSON.parse(data);
					if(data.result)
					{
						alert("Saved");
					}
					else
					{
						alert("Error occured");
					}
				}
	  });

	}); 
	});</script>';
    
}

