<?php
function incrementalHash($len = 5){
  $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $result = '';
    for ($i = 0; $i < $len; $i++)
        $result .= $charset[mt_rand(0, 35)];
	
	return $result;
}

function wspsc_show_coupon_discount_settings_page()
{
    if(!current_user_can('manage_options')){
        wp_die('You do not have permission to access this settings page.');
    }
	
	global $wpdb;
    $coupons_used = $wpdb->get_results('SELECT p.post_date, pm.post_id, pm.meta_value FROM bc_postmeta AS pm LEFT JOIN bc_posts AS p ON p.ID=pm.post_id WHERE pm.meta_key="coupon" AND pm.meta_value<>"" AND pm.post_id IN (SELECT post_id FROM bc_postmeta WHERE meta_key="wpsc_status" AND meta_value="Completed") GROUP BY pm.post_id', ARRAY_A);
	$post_ids = array_column($coupons_used, 'post_id');
	$coupon_names = array_column($coupons_used, 'meta_value');
	
	$coup_count = array();
	
	foreach($coupon_names as $cname)
	{
		if(!isset($coup_count[$cname]))
			$coup_count[$cname] = 1;
		else
			$coup_count[$cname]++;
	}
	
    echo '<div class="wrap">';
    echo '<h1>' . (__("Simple Cart Coupons/Discounts", "wordpress-simple-paypal-shopping-cart")) . '</h1>';
    
    echo '<div id="poststuff"><div id="post-body">';
    
    if (isset($_POST['wpspsc_coupon_settings']))
    {
        $nonce = $_REQUEST['_wpnonce'];
        if ( !wp_verify_nonce($nonce, 'wpspsc_coupon_settings')){
                wp_die('Error! Nonce Security Check Failed! Go back to Coupon/Discount menu and save the settings again.');
        }
        update_option('wpspsc_enable_coupon', (isset($_POST['wpspsc_enable_coupon']) && $_POST['wpspsc_enable_coupon']=='1') ? '1':'');
        echo '<div id="message" class="updated fade"><p><strong>';
        echo 'Coupon Settings Updated!';
        echo '</strong></p></div>';
    }
    if (isset($_POST['wpspsc_save_coupon']))
    {
        $nonce = $_REQUEST['_wpnonce'];
        if ( !wp_verify_nonce($nonce, 'wpspsc_save_coupon')){
                wp_die('Error! Nonce Security Check Failed! Go back to email settings menu and save the settings again.');
        }
        
        $collection_obj = WPSPSC_Coupons_Collection::get_instance();

        $coupon_code = trim(stripslashes(sanitize_text_field($_POST["wpspsc_coupon_code"])));
        $discount_rate = trim(sanitize_text_field($_POST["wpspsc_coupon_rate"]));
        $expiry_date = trim(sanitize_text_field($_POST["wpspsc_coupon_expiry_date"]));
		$isused = 0;
		if(isset($_POST["wpspsc_ismult"]))
			$ismult = 1;
		else
			$ismult = 0;
        $coupon_item = new WPSPSC_COUPON_ITEM($coupon_code, $discount_rate, $expiry_date, $isused, $ismult);
        $collection_obj->add_coupon_item($coupon_item);
        WPSPSC_Coupons_Collection::save_object($collection_obj);
        
        echo '<div id="message" class="updated fade"><p><strong>';
        echo 'Coupon Saved!';
        echo '</strong></p></div>';
    }
	
	if(isset($_POST['autogen_sub']))
    {
        $collection_obj = WPSPSC_Coupons_Collection::get_instance();

        $autogen_num = trim(stripslashes(sanitize_text_field($_POST["autogen_num"])));
		$autogen_rate = trim(sanitize_text_field($_POST["autogen_rate"]));
        $autogen_exp = addslashes($_POST["autogen_exp"]);
        if(isset($_POST["autogen_mult"]))
			$autogen_mult = 1;
		else
			$autogen_mult = 0;
		$isused = 0;

		for($i = 0; $i < $autogen_num; $i++)
		{
			$coupon_code = incrementalHash(7);
			$coupon_item = new WPSPSC_COUPON_ITEM($coupon_code, $autogen_rate, $autogen_exp, $isused, $autogen_mult);
			$collection_obj->add_coupon_item($coupon_item);
			WPSPSC_Coupons_Collection::save_object($collection_obj);
		}
        
        echo '<div id="message" class="updated fade"><p><strong>';
        echo $autogen_num.' coupon(s) have been generated!';
        echo '</strong></p></div>';
    }
    
    if(isset($_REQUEST['wpspsc_delete_coupon_id']) && (!isset($_POST['bdel']) || empty($_POST['bdel'])))
    {
        $coupon_id = $_REQUEST['wpspsc_delete_coupon_id'];
        $collection_obj = WPSPSC_Coupons_Collection::get_instance();
        $collection_obj->delete_coupon_item_by_id($coupon_id);
        echo '<div id="message" class="updated fade"><p>';
        echo 'Coupon successfully deleted!';
        echo '</p></div>';
    }
	
	if(isset($_POST['bdel']))
    {
		$todel = $_POST['todel'];
		$collection_obj = WPSPSC_Coupons_Collection::get_instance();
		foreach($todel as $did)
		{
			$collection_obj->delete_coupon_item_by_id($did);
		}
	}
	
    $wpspsc_enable_coupon = '';
    if (get_option('wpspsc_enable_coupon') == '1'){
        $wpspsc_enable_coupon = 'checked="checked"';
    }
    ?>
    
    <div class="wspsc_yellow_box">	
    <p><?php _e("For more information, updates, detailed documentation and video tutorial, please visit:", "wordpress-simple-paypal-shopping-cart"); ?><br />
    <a href="https://www.tipsandtricks-hq.com/wordpress-simple-paypal-shopping-cart-plugin-768" target="_blank"><?php _e("WP Simple Cart Homepage", "wordpress-simple-paypal-shopping-cart"); ?></a></p>
    </div>
    
    <form method="post" action="">
    <?php wp_nonce_field('wpspsc_coupon_settings'); ?>
    <input type="hidden" name="coupon_settings_update" id="coupon_settings_update" value="true" />

    <div class="postbox">
    <h3 class="hndle"><label for="title"><?php _e("Coupon/Discount Settings", "wordpress-simple-paypal-shopping-cart");?></label></h3>
    <div class="inside">

    <form method="post" action="">
    <table class="form-table" width="100%" border="0" cellspacing="0" cellpadding="6">
    
    <tr valign="top">
    <th scope="row"><?php _e("Enable Discount Coupon Feature", "wordpress-simple-paypal-shopping-cart");?></th>
    <td>
    <input type="checkbox" name="wpspsc_enable_coupon" value="1" <?php echo $wpspsc_enable_coupon; ?> />
    <span class="description"><?php _e("When checked your customers will be able to enter a coupon code in the shopping cart before checkout.", "wordpress-simple-paypal-shopping-cart");?></span>
    </td>
    </tr>
    
    <tr valign="top">
    <th scope="row">
    <div class="submit">
        <input type="submit" name="wpspsc_coupon_settings" class="button-primary" value="<?php echo (__("Update &raquo;", "wordpress-simple-paypal-shopping-cart")) ?>" />
    </div>
    </th>
    <td></td>
    </tr>

    </table>

    </form>
    </div></div>

    <form method="post" action="">
    <?php wp_nonce_field('wpspsc_save_coupon'); ?>
    <input type="hidden" name="info_update" id="info_update" value="true" />

    <div class="postbox">
    <h3 class="hndle"><label for="title"><?php _e("Add Coupon/Discount", "wordpress-simple-paypal-shopping-cart");?></label></h3>
    <div class="inside">

    <form method="post" action="">
    <table class="form-table" border="0" cellspacing="0" cellpadding="6" style="max-width:650px;">

    <tr valign="top">
    <td width="20%" align="left">
    Coupon Code<br />
    <input name="wpspsc_coupon_code" type="text" size="15" value=""/>   
    </td>

    <td width="20%" align="left">
    Discount Rate (%)<br />
    <input name="wpspsc_coupon_rate" type="text" size="7" value=""/>            
    </td>
    
    <td width="20%" align="left">
    Expiry Date<br />
    <input name="wpspsc_coupon_expiry_date" class="wpspsc_coupon_expiry" type="text" size="15" value=""/>            
    </td>
	<td width="20%" align="left">
    Multiple?<br />
	<input type="checkbox" name="wpspsc_ismult" class="wpspsc_ismult">          
    </td>
    <td width="20%" align="left">
    <div class="submit">
        <input type="submit" name="wpspsc_save_coupon" class="button-primary" value="<?php echo (__("Save Coupon &raquo;", "wordpress-simple-paypal-shopping-cart")) ?>" />
    </div>                
    </td> 

    </tr>

    </table>

    </form>
    </div></div>
    
    <?php
    
    //display table
    $output = "";
    $output .= '
	<form method="POST">
    <table class="widefat" style="max-width:800px;">
    <thead><tr>
	 <th scope="col"></th>
    <th scope="col">'.(__("Coupon Code", "wordpress-simple-paypal-shopping-cart")).'</th>
	<th scope="col">TIMES USED</th>
    <th scope="col">'.(__("Discount Rate (%)", "wordpress-simple-paypal-shopping-cart")).'</th>
    <th scope="col">'.(__("Expiry Date", "wordpress-simple-paypal-shopping-cart")).'</th>  
	<th scope="col">'.(__("Used?", "wordpress-simple-paypal-shopping-cart")).'</th> 	
	<th scope="col">'.(__("Multiple?", "wordpress-simple-paypal-shopping-cart")).'</th> 	
    <th scope="col"></th>
    </tr></thead>
    <tbody>';

    $collection_obj = WPSPSC_Coupons_Collection::get_instance();
    if($collection_obj)
    {
        $coupons = $collection_obj->coupon_items; 
        $number_of_coupons = count($coupons);
        if($number_of_coupons > 0)
        {
            $row_count = 0;
            foreach ($coupons as $coupon)
            {
				$times_used  = isset($coup_count[$coupon->coupon_code]) ? $coup_count[$coupon->coupon_code] : '-';
                $output .= '<tr>';
				 $output .= '<td><input type="checkbox" name="todel[]" value="'.$coupon->id.'"></td>';
                $output .= '<td><strong>'.$coupon->coupon_code.'</strong></td>';
				 $output .= '<td><strong>'.$times_used.'</strong></td>';
                $output .= '<td><strong>'.$coupon->discount_rate.'</strong></td>';
                if(empty($coupon->expiry_date)){
                    $output .= '<td><strong><input class="wpspsc_coupon_expiry tbl" id="'.$coupon->id.'" name="exd_'.$coupon->id.'" value="No Expiry"></strong></td>';
                }else{
                    $output .= '<td><strong><input class="wpspsc_coupon_expiry tbl" id="'.$coupon->id.'" name="exd_'.$coupon->id.'" value="'.$coupon->expiry_date.'"></strong></td>';
                }
				 $output .= '<td><strong>'.$coupon->isused.'</strong></td>';
				 $output .= '<td><strong>'.$coupon->ismult.'</strong></td>';
                $output .= '<td>';
                $output .= "<form method=\"post\" action=\"\" onSubmit=\"return confirm('Are you sure you want to delete this entry?');\">";				
                $output .= "<input type=\"hidden\" name=\"wpspsc_delete_coupon_id\" value=".$coupon->id." />";
                $output .= '<input style="border: none; color: red; background-color: transparent; padding: 0; cursor:pointer;" type="submit" name="Delete" value="Delete">';
                $output .= "</form>";
                $output .= '</td>';
                $output .= '</tr>';
                $row_count = $row_count + 1;
            }
        }
        else
        {
            $output .= '<tr><td colspan="5">'.(__("No Coupons Configured.", "wordpress-simple-paypal-shopping-cart")).'</td></tr>';
        }
    }
    else
    {
        $output .= '<tr><td colspan="5">'.(__("No Record found", "wordpress-simple-paypal-shopping-cart")).'</td></tr>';
    }

	$output .= '<tr><td colspan="5"><input type="submit" name="bdel" value="Group delete" style="padding:5px; padding-bottom:25px; width:150px; height:10px; border:1px solid #6b7077; background:#fff"></td></tr>';
    $output .= '</tbody>
    </table></form>';
	
	$output .= '<div style="margin-top:10px;"><a style="display:block; padding:5px; padding-bottom:15px; text-align:center; width:150px; height:10px; text-decoration:none; border:1px solid #6b7077; background:#fff" href="'.plugins_url().'/wordpress-simple-paypal-shopping-cart/cexport.php">Export CSV</a></div>';
	
	$output .= '<div style="margin-top:10px;">
	Automatic coupon generation:<br>
	<form method="post">
	<table>
		<tr>
			<td>Number: <input name="autogen_num" type="text" size="15"></td>
			<td>Rate: <input name="autogen_rate" type="text" size="15"></td>
			<td>Expiry date: <input name="autogen_exp" class="wpspsc_coupon_expiry" type="text"></td>
			<td>Is multiple? <input name="autogen_mult" type="checkbox"></td>
			<td><input name="autogen_sub" type="submit" value="Generate!"></td>
		</tr>
	</table>
	</form>
	</div>
	<script>
	$(document).ready(function(){
  $(".tbl").on("change",function postinput(){ 
    var matchvalue = $(this).val();
	var matchid = $(this).attr("id");
	jQuery.ajax({
			type: "POST",
			data: "action=coupddup&val="+matchvalue+"&id="+matchid,
			url: "'.site_url().'/wp-admin/admin-ajax.php",
			success: function(data)
			{   
			//alert("saved");
			}
  });
}); 
});</script>
	';

    echo $output;
    wpspsc_settings_menu_footer();
    
    echo '</div></div>';//End of poststuff and post-body
    echo '</div>';//End of wrap
    
}

